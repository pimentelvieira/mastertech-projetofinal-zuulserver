package com.mastertech.zuulserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class MastertechProjetofinalZuulserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechProjetofinalZuulserverApplication.class, args);
	}

}
