FROM openjdk:8-jre-alpine
COPY target/mastertech-projetofinal-zuulserver-0.0.1-SNAPSHOT.jar /zuul-server.jar
CMD ["/usr/bin/java", "-jar", "/zuul-server.jar"]
EXPOSE 9000